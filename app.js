const express = require('express');
const bodyParser = require('body-parser');
const cheerio = require('cheerio');
const PORT = process.env.PORT || 3000;
const routes = require('./routes');

// Set express framework
let app = express();

//  Connect all our routes to our application
app.use('/', routes);

app.listen(PORT);